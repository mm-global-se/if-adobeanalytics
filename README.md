# Adobe Analytics (Omniture, SiteCatalyst)

---

[TOC]

## Overview

+ This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to Adobe Analytics (previously known as Omniture, SiteCatalyst).

### How we send the data?

+ Our Adobe Analytics integrations uses specific eVar and prop variables, and populates them with the relevant user experience information. Each campaign sends its own data (a new siteCatalyst image). 


### Data format

The data sent to Adobe Analytics will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
) 


## Prerequisite
The following information needs to be provided by the client: 

+ Campaign Name

+ eVar and prop that needs to be used

+ Tracking Server/ Third Party Cookie


## Download

* [adobeanalytics-register.js](https://bitbucket.org/mm-global-se/if-adobeanalytics/src/master/src/adobeanalytics-register.js)

* [adobeanalytics-initialize.js](https://bitbucket.org/mm-global-se/if-adobeanalytics/src/master/src/adobeanalytics-initialize.js)


## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [adobeanalytics-register.js](https://bitbucket.org/mm-global-se/if-adobeanalytics/src/master/src/adobeanalytics-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Adobe Analytics Register scripts!

+ Create a campaign script and add the [adobeanalytics-initialize.js](https://bitbucket.org/mm-global-se/if-adobeanalytics/src/master/src/adobeanalytics-initialize.js) script. Customise the code by changing the campaign name,parameters, and trackingserver accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

 __NOTE!__  Change the value for “parameters: [‘’],” with the eVar and prop that you want to populate. (e.g if you want to use eVar1 and prop1 then the code will be “parameters: [‘eVar1’,’prop1’];”). Change the value of “trackingserver” to what is used on your site. Please consult with your development team to find out what this is set to. We check the tracking server to ensue that the data is sent to the correct data center. 



### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-adobeanalytics#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.


## QA

+ [Install](https://helpx.adobe.com/analytics/using/digitalpulse-debugger.html) the Adobe Digital Pulse Debugger

+ Navigate to the page where the campaign is running.

+ Open the Adobe Digital Pulse Debugger. Scroll down to find the eVar and prop value that you are populating. Please note that for multiple campaigns there will be multiple site catalyst images so carry on scrolling through the window to find all the values.

![omniture.jpg](https://bitbucket.org/repo/qypjjb/images/2999410857-omniture.jpg)

+ Make sure that the Report Suit ID, the Third Party Cookie, and the Visitor ID are the same in all Site Catalyst images 

## Common Problems

+ None currently known