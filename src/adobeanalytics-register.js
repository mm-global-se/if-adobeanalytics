mmcore.IntegrationFactory.register(
  'Adobe Analytics', {    
    defaults: {
        isStopOnDocEnd: false
    },
    validate: function(data){
        if (!data.campaign)
            return 'No campaign.';
        if (!data.parameters || !data.parameters.length)
            return 'No parameters.';
        return true;
    },
    check: function(data){
        return (window[data.sVariable || 's']
                && window[data.sVariable || 's'].hasOwnProperty('t'))
                    && s.trackingServer === data.trackingServer;       
    },
    exec: function(data){
        var s = window[data.sVariable || 's'],
            prodSand = data.isProduction ? 'MM_Prod' : 'MM_Sand',
            i = 0;
        if (!data.parameters.pop) data.parameters = [data.parameters];      
        s.linkTrackVars = data.parameters.join(',');      
        for (; i < data.parameters.length; i ++) {
            s[data.parameters[i]] = data.campaignInfo;
        }      
        s.tl(true, 'o', 'Maxymiser ' + prodSand);        
        if (data.callback) data.callback();
        return true;
    }
});
